from flask import Flask, render_template, redirect, url_for, abort

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return render_template('error500.html'), 500

@app.route('/')
def hello():
    return {'hola':'mundo Modificado'}

@app.route('/hola/<nombre>')
def hola(nombre):
    return render_template('base.html',name=nombre)

@app.route('/git')
def helloGit():
    return {'hola':'Estas trabajando en GIT'}

@app.route('/login')
def login():
    abort(500)